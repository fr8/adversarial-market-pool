var AMPToken2 = artifacts.require('./AMPToken2.sol');
var TestRPC = require("ethereumjs-testrpc"); // TestRPC is now Ganache must update this part 

var val = web3.toWei(1, 'ether');
console.log(val)

contract('AMPToken2', function (accounts) {
    var AMP2 = AMPToken2.deployed();
    var account_arr = accounts;
    //console.log(web3.eth.getBalance(account_arr[0]).valueOf());

    it('should correctly start a new game when the time is up', function (accounts) {
        AMP2.then(function (instance) {
            return instance.back('A', { 'from': account_arr[1], 'value': web3.toWei(0.5, 'ether') });
        }).then(function (tx) {
            printSides(AMP2, 0);
            printMyAmts(AMP2, account_arr[1], 1);
            AMP2.then(function (instance) {
                return instance.back('A', { 'from': account_arr[2], 'value': web3.toWei(0.3, 'ether') });
            }).then(function (tx) {
                printSides(AMP2, 0);
                printMyAmts(AMP2, account_arr[2], 2);
                AMP2.then(function (instance) {
                    return instance.back('B', { 'from': account_arr[3], 'value': web3.toWei(0.7, 'ether') });
                }).then(function (tx) {
                    printSides(AMP2, 0);
                    printMyAmts(AMP2, account_arr[3], 3);
                    AMP2.then(function (instance) {
                        return instance.back('B', { 'from': account_arr[4], 'value': web3.toWei(0.05, 'ether') });
                    }).then(function (tx) {
                        printSides(AMP2, 0);
                        printMyAmts(AMP2, account_arr[4], 4);
                        console.log(1);
                        /*
                        *now try to start a new game
                        */
                        AMP2.then(function (instance) {
                            console.log(2);
                            return instance.isGameRunning.call(0);
                        }).then(function (running) {
                            console.log("GAME RUNNING: " + running);
                            console.log("WAITING.....");
                        })
                        setTimeout(function () {
                            AMP2.then(function (instance) {
                                return instance.getGameWinner.call(0);
                            }).then(function (winner) {
                                console.log("WINNER: " + winner)
                            });
                            printBal(account_arr[1], 1);
                            printBal(account_arr[2], 2);
                            printBal(account_arr[3], 3);
                            printBal(account_arr[4], 4);
                            AMP2.then(function (instance) {
                                return instance.startGame({ 'from': account_arr[4] })
                            }).then(function (tx) {
                                console.log("New game started")
                                AMP2.then(function (instance) {
                                    return instance.getCurGameId.call()
                                }).then(function (id) {
                                    console.log("GAME ID: " + id);
                                })
                                AMP2.then(function (instance) {
                                    return instance.getGameWinner.call(0);
                                }).then(function (winner) {
                                    console.log("WINNER: " + winner)
                                    assert.equal(winner, 'A', 'winner not correct')
                                });
                                printBal(account_arr[1], 1);
                                printBal(account_arr[2], 2);
                                printBal(account_arr[3], 3);
                                printBal(account_arr[4], 4);
                                //See the treasury bal:
                                AMP2.then(function (instance) {
                                    return instance.getTreasury.call()
                                }).then(function (val) {
                                    console.log("Treasury contains: " + web3.fromWei(val, 'ether'))
                                });
                                //See how much the bounty was:
                                printValFlagsEth(AMP2);
                                //See if the bounty sent:
                                printBoolFlags(AMP2);
                                /*
                                * Now try some withdrawals
                                */
                                AMP2.then(function (instance) {
                                    return instance.withdrawWinnings(0, { 'from': account_arr[1] });
                                }).then(function (tx) {
                                    printBoolFlags(AMP2);
                                    printValFlagsEth(AMP2);
                                    printBal(account_arr[1], 1);
                                })
                            })
                        }, 80000)
                    })
                })
            })
        })
    })
});

function printBal(account, index) {
    console.log("ACC " + index + ": " + web3.fromWei(web3.eth.getBalance(account).valueOf(), 'ether'));
}

function printSides(contract, index) {
    contract.then(function (instance) {
        return instance.getTotalInA.call(index);
    }).then(function (tot) {
        console.log("SIDE A TOT: " + tot);
    });
    contract.then(function (instance) {
        return instance.getTotalInB.call(index);
    }).then(function (tot) {
        console.log("SIDE B TOT: " + tot);
    });
}

function printMyAmts(contract, account, index) {
    contract.then(function (instance) {
        return instance.getMyAmtInA.call(0, { 'from': account });
    }).then(function (tot) {
        console.log(index + " A TOT: " + tot.valueOf());
    });
    contract.then(function (instance) {
        return instance.getMyAmtInB.call(0, { 'from': account });
    }).then(function (tot) {
        console.log(index + " B TOT: " + tot.valueOf());
    });
}

function printBoolFlags(contract) {
    contract.then(function (instance) {
        return instance.flagA.call();
    }).then(function (flag) {
        console.log("FLAG A: " + flag);
    });

    contract.then(function (instance) {
        return instance.flagB.call();
    }).then(function (flag) {
        console.log("FLAG B: " + flag);
    });

    contract.then(function (instance) {
        return instance.flagC.call();
    }).then(function (flag) {
        console.log("FLAG C: " + flag);
    });
}

function printValFlags(contract) {
    contract.then(function (instance) {
        return instance.valFlagA.call();
    }).then(function (flag) {
        console.log("VAL FLAG A: " + flag);
    });

    contract.then(function (instance) {
        return instance.valFlagB.call();
    }).then(function (flag) {
        console.log("VAL FLAG B: " + flag);
    });

    contract.then(function (instance) {
        return instance.valFlagC.call();
    }).then(function (flag) {
        console.log("VAL FLAG C: " + flag);
    });
}

function printValFlagsEth(contract) {
    contract.then(function (instance) {
        return instance.valFlagA.call();
    }).then(function (flag) {
        console.log("VAL FLAG A: " + web3.fromWei(flag, 'ether'));
    });

    contract.then(function (instance) {
        return instance.valFlagB.call();
    }).then(function (flag) {
        console.log("VAL FLAG B: " + web3.fromWei(flag, 'ether'));
    });

    contract.then(function (instance) {
        return instance.valFlagC.call();
    }).then(function (flag) {
        console.log("VAL FLAG C: " + web3.fromWei(flag, 'ether'));
    });
}
